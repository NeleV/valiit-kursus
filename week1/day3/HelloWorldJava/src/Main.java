
public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World!"); // console.log
        System.out.println("sout"); // sout on kiirkäsk
        ; // iga rea lõpus peab olema semikoolon

        int number = 5;  // täisarv ehk integer
        double n2 = 5.7; // komakohaga (ja oluliselt laiema ulatusega) number

        String nimi = "Krister"; // Sõne
        char algusTaht = 'K';    // üksik täht

        if (number == 5) { // if süntaks on täpselt sama mis JSis
        } else {
        }

        int liitmine = (int) (number + n2); // cast-in ühest tüübist teise
        System.out.println(liitmine);
        System.out.println(Math.round(10.7)); // ümardamiseks on eraldi meetod

        int parisNumber = Integer.parseInt("4");
        double parisNumber2 = Double.parseDouble("4.1");

        String puuvli1 = "Banaan";
        String puuvli2 = "Banaan";
        if (puuvli1.equals(puuvli2)) {
            System.out.println("PUUVLID ON VÕRDSED!!!");
        } else {
            System.out.println("EI OLE VÕRDSED");
        }

        Koer.auh();
        Koer.lausu();
        Kass.nurr();
    }

    public static int summa(int a, int b) {
        return a + b;
    }
}














