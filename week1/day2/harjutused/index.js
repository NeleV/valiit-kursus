console.log("hommik!")

var sisend = 7
var tulemus

// Kui vähem kui 7, korruta kahega
// Kui suurem kui 7, jaga kahega
// Kui ongi täpselt 7, jäta samaks

if (sisend == 7) {
	tulemus = sisend
} else if (sisend < 7) {
	tulemus = sisend * 2
} else {
	tulemus = sisend / 2
}

console.log("Tulemus on: " + tulemus)

/*
	Kui sõned on võrdsed, siis prindi üks,
	kui erinevad, siis liida kokku ja prindi.
*/

var str1 = "banaan"
var str2 = "apelsin"

if (str1 == str2) {
	console.log(str1)
} else {
	console.log(str1 + " " + str2)
}

/*
	Meil on linnade nimekiri, aga ilma sõnata
	"linn". Need palun lisada.
*/

var linnad = ["Tallinn", "Tartu", "Valga"] // Defineerime nimekirja
var uuedLinnad = [] // Defineerime nimekirja kuhu tulemused panna

while (linnad.length > 0) {  // Kuni linnasid on listis
	var linn = linnad.pop()  // võta välja viimane
	var uusLinn = linn + " linn" // ja lisa "linn" otsa
	uuedLinnad.push(uusLinn)  // tulemus salvesta uude listi
}

console.log(uuedLinnad)

/*
	Eralda poiste ja tüdrukuta nimed
*/

var nimed = ["Margarita", "Mara", "Matrin", "Kalev"]
var poisteNimed = []
var tydrukuteNimed = []

while (nimed.length > 0) {
	// "a" lõpuga on tüdruku nimi. Googelda kuidas seda küsida.
	// Kui on poisi nimi, lisa see poisteNimed listi
	// Kui on tüdruku nimi, lisa see tydrukuteNimed listi
	var nimi = nimed.pop()
	if (nimi.endsWith("a")) {
		tydrukuteNimed.push(nimi)
	} else {
		poisteNimed.push(nimi)
	}
}
console.log(poisteNimed, tydrukuteNimed)

// ctrl + klikk saad mitu kursorit
// ctrl + enter tekitad uue rea
// console.log("muutuja", muutuja) prindib loetavalt infot

/* FUNKTSIOONID */

/*
	Kirjuta algoritm, mis suudab ükskõik mis naise/mehe
	nime eristada.
*/

var eristaja = function(nimi) {
	if (nimi.endsWith("a")) {
		return "tüdruk"
	} else {
		return "poiss"
	}
}

console.log(eristaja("Peeter"))
console.log(eristaja("Margarita"))
console.log(eristaja("Krister"))

/*
	Loo funktsioon, mis tagastab vastuse küsimusele,
	kas tegu on numbriga?
	!isNaN(4) ehk is Not a Number. Hüüumärk pöörab true/false vastupidi.
*/

var kasOnNumber = function(number) {
	if (!isNaN(number)) {
		return true
	}
	return false
}

console.log( kasOnNumber(4) )
console.log( kasOnNumber("mingi sõne") )
console.log( kasOnNumber(23536) )
console.log( kasOnNumber(6.876) )
console.log( kasOnNumber(null) )
console.log( kasOnNumber([1, 4, 5, 6]) )

/*
	Kirjuta functioon, mis võtab vastu kaks numbrit
	ja tagastab nende summa.
*/

var summa = function(a, b) {
	return a + b
}

console.log(summa(4, 5))
console.log(summa(7, 87))

/*

*/
console.log("---------------")

var inimesed = {
	kaarel: 34,
	"Margarita": 10,
	"Suksu": [3, 4, 5],
	"Krister": {
		vanus: 30,
		sugu: true
	}
}

console.log(inimesed["kaarel"])
console.log(inimesed.kaarel)
console.log(inimesed.Krister.sugu)
console.log(inimesed.Suksu[1])