
# Vali IT kursus 25.02.2019

Esimesed kolm nädalat kuue nädalasest kursusest annan mina, Krister. Ma kogun koodinäited ja materjalid siia ühte kohta. Lisaks materjalidele üritan ka logida tegevusi ja küsimusi, mida arvesse võtta järgmisel kursusel.

## Päevakava

- 9:15 Kordamine
- 9:45 Teooria
- **10:15 Paus**
- 10:30 Praktika
- **12:00 Lõuna**
- 13:00 Praktika
- **15:15 Paus**
- 15:30 Iseseisev töö
- **16:15 Iseseisev töö kodus**

## Linke

## Õpetusi

- [Giti kasutamine](https://www.youtube.com/watch?v=P1s2_MQqskQ)
- [IT Kolledži videod](https://www.youtube.com/user/krivar/videos)

### Install

- [Slacki invite](https://join.slack.com/t/valiit25022019/shared_invite/enQtNTU4NTk3MTg1NDA4LWJmMTA1YzNiZjNmY2NjYmVkZWZiZTc0NTFlZDA1NmZkNjQ3NGI4MzRmNjdiYzMyNzgyYzlkYTEyODAxMGQ5ODY)
- [Giti windowsi install](https://git-scm.com/download/win)
- [Sublime Text 3](https://www.sublimetext.com/3)
- [Java](https://download.java.net/java/GA/jdk11/9/GPL/openjdk-11.0.2_windows-x64_bin.zip)
- [Intellij](https://www.jetbrains.com/idea/download/download-thanks.html?platform=windows&code=IIC)
- [PostgreSQL](https://mvnrepository.com/artifact/postgresql/postgresql/9.1-901-1.jdbc4)
. [Õpsile Wacom driver](http://downloadeu.wacom.com/pub/WINDOWS/PenTablet_5.3.5-3.exe)

### Muu

- [i200 Git käsud](http://i200.itcollege.ee/#Git)
- [Spring Boot init](https://start.spring.io/)